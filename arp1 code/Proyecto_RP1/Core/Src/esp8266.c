/* Copyright 2020, Grupo 5-TD2-FRH-UTN
 * All rights reserved.
 *
 * This file is part of the Final Proyect ARP1.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#define _GNU_SOURCE //fixes memmem implicit declaration warning (even when string.h was included)
/*==================[inclusions]=============================================*/
#include "esp8266.h"
#include <ctype.h>
#include "main.h"
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include "arp1.h"
/*==================[macros and definitions]=================================*/
#define ESP_TIMEOUT 20000
#define BUF_LEN 128
/*==================[internal data declaration]==============================*/
static uint32_t tx_len;
UART_HandleTypeDef huart2;
static char tx_buffer[BUF_LEN];
ESP8266_STATES esp_state;
uint8_t rx_buffer[BUF_LEN];
uint8_t cipesend[64];
HAL_StatusTypeDef rv;
char* p;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
static int32_t esp_timeout_ms;
static uint8_t cmd_rst[] = "AT+RST\r\n";
static uint8_t cmd_wifi_mode[] = "AT+CWMODE=3\r\n";
//static uint8_t cmd_wifi_connect[] = "AT+CWJAP=\"Fibertel WiFi448 2.4GHz\",\"decada2010\"\r\n";
static uint8_t cmd_wifi_connect[] = "AT+CWJAP=\"HITRON-3480\",\"JLX4NDZ53BQF\"\r\n";
static uint8_t cmd_udp_init[] = "AT+CIPSTART=\"UDP\",\"192.168.0.14\",8080,55555\r\n";
//192.168.4.1 TCP
//192.168.0.24 UDP1
//192.168.0.16 UDP2
static uint8_t flag;
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void esp8266_tick(void)
{
	esp_timeout_ms++;
	if (esp_timeout_ms >= ESP_TIMEOUT)
	{
		esp_timeout_ms = 0;
		esp_state = CMD_RST_FULL;
	}
}

int32_t send_data( void * data, uint32_t len)
{
	int32_t chk = -1;
	if (tx_len == 0)
	{
		memcpy(tx_buffer, data, len);
		tx_len = len;
		chk = len;
	}
	return chk;
}

static void reset_rx_buffer(void)
{
	huart2.pRxBuffPtr = rx_buffer;
	huart2.RxXferSize = BUF_LEN;
	huart2. RxXferCount = BUF_LEN;
	bzero(rx_buffer, BUF_LEN);
}

static void esp8266_send_task(void * a)
{

	while(1){

		HAL_UART_Receive_IT(&huart2,(uint8_t *)rx_buffer, sizeof(rx_buffer));

		switch(esp_state)
		{
		case CMD_RST_FULL:
			HAL_GPIO_WritePin(ESP_STOP_GPIO_Port, ESP_STOP_Pin, GPIO_PIN_SET);
			while(HAL_GPIO_ReadPin(ESP_FLAG_GPIO_Port, ESP_FLAG_Pin) == GPIO_PIN_RESET){
				esp_timeout_ms = 0;
				reset_rx_buffer();
			}
			HAL_GPIO_WritePin(ESP_STOP_GPIO_Port, ESP_STOP_Pin, GPIO_PIN_RESET);
			esp_state = CMD_RST_TX;
			flag = 0;
			break;
		case CMD_RST_TX:
			reset_rx_buffer();
			HAL_UART_Transmit_IT(&huart2, cmd_rst, strlen((const char*)cmd_rst));
			esp_state = CMD_RST_RX;
			break;
		case CMD_RST_RX:
			if (memmem(rx_buffer, BUF_LEN, "OK", 2)){
				esp_state = CMD_CWMODE_TX;
				reset_rx_buffer();
			}
			break;
		case CMD_CWMODE_TX:
			HAL_UART_Transmit_IT(&huart2, cmd_wifi_mode , strlen((char*)cmd_wifi_mode));
			esp_state = CMD_CWMODE_RX;
			break;
		case CMD_CWMODE_RX:
			if (memmem(rx_buffer, BUF_LEN, "OK", 2)){
				esp_state = CMD_CWJAP_TX;
				reset_rx_buffer();
			}
			break;
		case CMD_CWJAP_TX:
			reset_rx_buffer();
			HAL_UART_Transmit_IT(&huart2, cmd_wifi_connect, strlen((char*)cmd_wifi_connect));
			esp_state = CMD_CWJAP_RX;
			break;
		case CMD_CWJAP_RX:
			if (memmem(rx_buffer, 128, "WIFI CONNECTED", 14) &&
					memmem(rx_buffer, 128, "WIFI GOT IP", 11)){
				esp_state = CMD_CIPSTART_TX;
			}
			break;
		case CMD_CIPSTART_TX:
			reset_rx_buffer();
			flag = 1;
			HAL_UART_Transmit_IT(&huart2, cmd_udp_init, strlen((char*)cmd_udp_init));
			esp_state = CMD_CIPSTART_RX;
			break;
		case CMD_CIPSTART_RX:
			if (memmem(rx_buffer, 128, "CONNECT", 7) && (memmem(rx_buffer, 128, "OK", 2))){
				reset_rx_buffer();
				esp_state = CMD_SEND_SIZE_TX;
				HAL_GPIO_WritePin(ESP_GO_GPIO_Port, ESP_GO_Pin, GPIO_PIN_SET);
			}
			break;
		case CMD_SEND_SIZE_TX:
			esp_timeout_ms = 0;
			if (tx_len){
				snprintf((char*)cipesend, 64, "AT+CIPSEND=%lu\r\n", tx_len);
				reset_rx_buffer();
				HAL_UART_Transmit(&huart2, (uint8_t *) cipesend, strlen((char*)cipesend), 500);
				esp_state = CMD_SEND_SIZE_RX;
			}else{

				if ((p = (char*) memmem(rx_buffer, BUF_LEN, "+IPD,", 5)) && memchr(rx_buffer, ':', BUF_LEN)){
					vTaskDelay(200);
					data_rved(rx_buffer, BUF_LEN, p);
					if(dist_flag_rx()){
						give_semp_RX();
					}
					reset_rx_buffer();
				}
			}
			break;
		case CMD_SEND_SIZE_RX:
			esp_timeout_ms = 0;
			if (memchr(rx_buffer, '>', BUF_LEN)){
				if (HAL_OK == HAL_UART_Transmit_IT(&huart2,(uint8_t*) tx_buffer, tx_len)){
					reset_rx_buffer();
					tx_len = 0;
					esp_state = CMD_SEND_SIZE_TX;
				}
			}
		default:
			break;
		}
		taskYIELD();
	}
}

void esp8266_init(void)
{
	TaskHandle_t indicador;

	HAL_GPIO_WritePin(ESP_PD_GPIO_Port, ESP_PD_Pin, GPIO_PIN_SET);

	xTaskCreate(esp8266_send_task, "esp8266_send", configMINIMAL_STACK_SIZE*2,
			NULL, tskIDLE_PRIORITY+1, &indicador);
	bzero(rx_buffer, 128);
	bzero(tx_buffer, 128);
	esp_timeout_ms = 0;
	reset_rx_buffer();
	flag = 0;
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if(flag){
		give_semp_TX();
	}
}
/*==================[end of file]============================================*/

