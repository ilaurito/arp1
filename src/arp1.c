/* Copyright 2020, Grupo 5-TD2-FRH-UTN
 * All rights reserved.
 *
 * This file is part of the Final Proyect ARP1..
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#define _GNU_SOURCE

/*==================[inclusions]=============================================*/
#include "arp1.h"
#include "main.h"
#include <ctype.h>
#include "stdio.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "esp8266.h"
#include "tb6612fng.h"
#include "HC-SR04.h"
#include "hall_sensor.h"
#include "pir.h"
#include "mpu6050.h"

/*==================[macros and definitions]=================================*/
#define STACK_SIZE 5
#define BUF_LEN 128
#define MAX_TOUR 8
#define ARP1_TIMEOUT 4000
#define ANG_RIGHT -90
#define ANG_RIGHT_INV 270
#define ANG_LEFT 90
#define ANG_BACK 180
#define ANG_BACK_INV -180
#define ANG_FORW 0
#define TURNINGRIGHT 0
#define TURNINGLEFT 1

/*==================[internal data declaration]==============================*/
static char tx_buffer[BUF_LEN];
static char rx_buffer[BUF_LEN];

QueueHandle_t queue_tx, queue_rx;
static SemaphoreHandle_t semp_uart_TX;
static SemaphoreHandle_t semp_uart_RX;

CTRL_STATES ctrl_state;

static int8_t tour;
static int8_t tour_aux;
static uint8_t turning_direction;
static uint16_t uart_distance;
static uint16_t distance_travelled;
static uint16_t required_distance;
static uint16_t error_distance;
static direction_states next_direction;
static int32_t arp1_timeout_ms;
static uint8_t max_tours;
static uint8_t flag_no_detection;
static uint8_t flag_rotation;
static uint8_t flag_arp1_timer;
static uint8_t flag_enable_measure;
static int16_t gyro_angle_arp1;
static int num;
static uint8_t go_left;
static uint8_t go_right;
static uint8_t motors_on;

typedef struct{
	direction_states current_state;
	uint16_t distance;
}arp1_tour_t;

arp1_tour_t arp1_tour[MAX_TOUR];
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/*==================[internal functions definition]==========================*/
void arp1_tick(void)
{
	if(flag_arp1_timer){
		arp1_timeout_ms++;
		if (arp1_timeout_ms >= ARP1_TIMEOUT){
			arp1_timeout_ms = 0;
			flag_arp1_timer = 0;
			flag_no_detection = 1;
		}
	}
}

static void tx_task(void *a)
{
	while(1)
	{
		xQueueReceive(queue_tx, tx_buffer, portMAX_DELAY);
		if(xSemaphoreTake(semp_uart_TX, portMAX_DELAY) == pdTRUE)
		{
			send_data(tx_buffer, strlen(tx_buffer));
		}
	}
}

static void arp1_task(void * a)
{
	static direction_states direction_s;
	char tx_data[64];
	while(1){
		switch(ctrl_state){
		case WELCOME:
			snprintf(tx_data, BUF_LEN, "Bienvenidos! Soy Arp1");
			xQueueSend(queue_tx, tx_data,0);
			snprintf(tx_data, BUF_LEN, "Configuremos mi recorrido");
			xQueueSend(queue_tx, tx_data,0);
			snprintf(tx_data, BUF_LEN, "Ingrese Y para comenzar");
			xQueueSend(queue_tx, tx_data,0);
			ctrl_state = SETTINGS;
			break;

		case SETTINGS:
			if(memmem(rx_buffer, BUF_LEN, "Y", 1)){
				gyro_drift_flag();
				ctrl_state = HOWFAR_TX;
				tour = 0;
				direction_s = FORWARD;
			}
			break;

		case WHERETO_TX:
			snprintf(tx_data, BUF_LEN, "Indique direccion");
			xQueueSend(queue_tx, tx_data,0);
			snprintf(tx_data, BUF_LEN, "El camino debe cerrarse");
			xQueueSend(queue_tx, tx_data,0);
			snprintf(tx_data, BUF_LEN, "(AD)Adelante, (BA)Atras, (DE)Derecha, (IZ)Izquierda, FIN");
			xQueueSend(queue_tx, tx_data,0);
			ctrl_state = WHERETO_RX;
			break;

		case WHERETO_RX:
			if(memmem(rx_buffer, BUF_LEN, "AD", 2)){
				tour++;
				if(direction_s == BACKWARD){direction_s = BACKWARD;}
				else if(direction_s == RIGHT){direction_s = RIGHT;}
				else if(direction_s == LEFT){direction_s = LEFT;}
				else if(direction_s == FORWARD){direction_s = FORWARD;}
				ctrl_state = HOWFAR_TX;
			}
			else if(memmem(rx_buffer, BUF_LEN, "BA", 2)){
				tour++;
				if(direction_s == BACKWARD){direction_s = FORWARD;}
				else if(direction_s == RIGHT){direction_s = LEFT;}
				else if(direction_s == LEFT){direction_s = RIGHT;}
				else if(direction_s == FORWARD){direction_s = BACKWARD;}
				ctrl_state = HOWFAR_TX;
			}
			else if(memmem(rx_buffer, BUF_LEN, "DE", 2)){
				tour++;
				if(direction_s == BACKWARD){direction_s = LEFT;}
				else if(direction_s == RIGHT){direction_s = BACKWARD;}
				else if(direction_s == LEFT){direction_s = FORWARD;}
				else if(direction_s == FORWARD){direction_s = RIGHT;}
				ctrl_state = HOWFAR_TX;
			}
			else if(memmem(rx_buffer, BUF_LEN, "IZ", 2)){
				tour++;
				if(direction_s == BACKWARD){direction_s = RIGHT;}
				else if(direction_s == RIGHT){direction_s = FORWARD;}
				else if(direction_s == LEFT){direction_s = BACKWARD;}
				else if(direction_s == FORWARD){direction_s = LEFT;}
				ctrl_state = HOWFAR_TX;
			}
			else if(memmem(rx_buffer, BUF_LEN, "FIN", 3)){
				ctrl_state = BEGIN_TX;
			}
			break;

		case HOWFAR_TX:
			snprintf(tx_data, BUF_LEN, "Ingrese distancia en Centimetros");
			xQueueSend(queue_tx, tx_data,0);
			if(xSemaphoreTake(semp_uart_RX, portMAX_DELAY) == pdTRUE)
				ctrl_state = HOWFAR_RX;
			break;

		case HOWFAR_RX:
			arp1_tour[tour].current_state = direction_s;
			arp1_tour[tour].distance = uart_distance;
			uart_distance = 0;
			ctrl_state = WHERETO_TX;
			bzero(rx_buffer, BUF_LEN);
			break;

		case BEGIN_TX:
			snprintf(tx_data, BUF_LEN, "ESCRIBA START PARA COMENZAR");
			xQueueSend(queue_tx, tx_data,0);
			max_tours = tour;
			ctrl_state = BEGIN_RX;
			break;

		case BEGIN_RX:
			if(memmem(rx_buffer, BUF_LEN, "START", 5)){
				ctrl_state = PATROL;
				tour = 0;
				required_distance = arp1_tour[tour].distance;
				bzero(rx_buffer, BUF_LEN);
			}
			break;

		case PATROL:
			arp1_timeout_ms = 0;
			vTaskDelay(200);
			give_semp_HCSR04();
			if((HC_SR04_objeto_cercano() == 1) && flag_enable_measure){
				motor_brake();
				snprintf(tx_data, BUF_LEN, "Objeto cercano");
				xQueueSend(queue_tx, tx_data,0);
				ctrl_state = SCAN;
				flag_arp1_timer = 1;
			}
			else{
				motor_forward();
				distance_travelled = hall_dist_recorrida();
				if(tour == max_tours){
					tour = -1;
				}
				if((required_distance - distance_travelled) < error_distance){
					motor_brake();
					next_direction = arp1_tour[tour+1].current_state;
					required_distance = arp1_tour[tour+1].distance;
					flag_enable_measure = 1;
					flag_rotation = 1;
					ctrl_state = SCAN;
				}
			}
			break;

		case SCAN:
			vTaskDelay(1000);
			if(pir_detection()){
				snprintf(tx_data, BUF_LEN, "ALERTA INTRUSO %d", num++);
				xQueueSend(queue_tx, tx_data,0);
				vTaskDelay(750);
				if(flag_rotation)
				{
					ctrl_state = ROTATION;
					flag_rotation = 0;
					vTaskDelay(1000);
				}else{
					ctrl_state = PATROL;
				}
				flag_arp1_timer = 0;
			}else if(flag_no_detection && flag_rotation){
				flag_arp1_timer = 0;
				flag_rotation = 0;
				ctrl_state = ROTATION;
			}else if(flag_no_detection && (required_distance - distance_travelled) > HC_SR04_distance()){
				flag_no_detection = 0;
				flag_arp1_timer = 0;
				ctrl_state = STOP;
			}
			else if(flag_no_detection && (required_distance - distance_travelled) < HC_SR04_distance()){
				flag_no_detection = 0;
				flag_arp1_timer = 0;
				flag_enable_measure = 0;
				ctrl_state = PATROL;
			}
			break;

		case ROTATION:
			gyro_angle_arp1 = gyro_angle();
			if(next_direction == FORWARD){
				if(!motors_on)
				{
					motors_on = 1;
					if(arp1_tour[tour_aux].current_state == RIGHT){
						turning_time(go_left);
						turning_direction = TURNINGLEFT;
					}else if(arp1_tour[tour_aux].current_state == BACKWARD || arp1_tour[tour_aux].current_state == LEFT){
						turning_time(go_right);
						turning_direction = TURNINGRIGHT;
					}
				}else{
					gyro_angle_arp1 = gyro_angle();
					if(turning_direction == TURNINGRIGHT && gyro_angle_arp1 <= ANG_FORW){
						go_to_patrol();
					}else if(turning_direction == TURNINGLEFT && gyro_angle_arp1 >= ANG_FORW){
						go_to_patrol();
					}
				}
			}
			else if(next_direction == RIGHT){
				if(!motors_on){
					motors_on = 1;
					if(arp1_tour[tour_aux].current_state == BACKWARD){
						turning_time(go_left);
						turning_direction = TURNINGLEFT;
					}else if(arp1_tour[tour_aux].current_state == FORWARD || arp1_tour[tour_aux].current_state == LEFT){
						turning_time(go_right);
						turning_direction = TURNINGRIGHT;
					}
				}else{
					gyro_angle_arp1 = gyro_angle();
					if(turning_direction == TURNINGRIGHT && gyro_angle_arp1 <= ANG_RIGHT){
						go_to_patrol();
					}else if(turning_direction == TURNINGLEFT && gyro_angle_arp1 >= ANG_RIGHT_INV){
						gyro_angle_drift_fix(arp1_tour[tour_aux].current_state);
						go_to_patrol();
					}
				}
			}
			else if(next_direction == LEFT){
				if(!motors_on){
					motors_on = 1;
					if(arp1_tour[tour_aux].current_state == FORWARD || arp1_tour[tour_aux].current_state == RIGHT ){
						turning_time(go_left);
						turning_direction = TURNINGLEFT;
					}else if(arp1_tour[tour_aux].current_state == BACKWARD){
						turning_time(go_right);
						turning_direction = TURNINGRIGHT;
					}
				}else{
					gyro_angle_arp1 = gyro_angle();
					if(turning_direction == TURNINGRIGHT && gyro_angle_arp1 <= ANG_LEFT){
						go_to_patrol();
					}else if(turning_direction == TURNINGLEFT && gyro_angle_arp1 >= ANG_LEFT){
						go_to_patrol();
					}
				}
			}
			else if(next_direction == BACKWARD){
				if(!motors_on){
					motors_on = 1;
					if(arp1_tour[tour_aux].current_state == RIGHT){
						turning_time(go_right);
						turning_direction = TURNINGRIGHT;
					}else if(arp1_tour[tour_aux].current_state == FORWARD || arp1_tour[tour_aux].current_state == LEFT){
						turning_time(go_left);
						turning_direction = TURNINGLEFT;
					}
				}else{
					gyro_angle_arp1 = gyro_angle();
					if(turning_direction == TURNINGRIGHT && gyro_angle_arp1 <= ANG_BACK_INV){
						gyro_angle_drift_fix(arp1_tour[tour_aux ].current_state);
						go_to_patrol();
					}else if(turning_direction == TURNINGLEFT && gyro_angle_arp1 >= ANG_BACK){
						go_to_patrol();
					}
				}
			}
			hall_reboot();
			break;

		case STOP:
			snprintf(tx_data, BUF_LEN, "OBJETO DEMASIADO CERCA");
			xQueueSend(queue_tx, tx_data,0);
			snprintf(tx_data, BUF_LEN, "IMPOSIBLE MOVERSE");
			xQueueSend(queue_tx, tx_data,0);
			vTaskDelay(2000);
			give_semp_HCSR04();
			if((HC_SR04_objeto_cercano() == 0)){
				ctrl_state = PATROL;
			}
			break;
		}
	}
}

void arp1_init(void)
{
	int i;
	num = 0;
	TaskHandle_t indicador;
	queue_rx = xQueueCreate(STACK_SIZE, sizeof(rx_buffer));
	queue_tx = xQueueCreate(STACK_SIZE, sizeof(tx_buffer));

	semp_uart_TX = xSemaphoreCreateBinary();
	semp_uart_RX = xSemaphoreCreateBinary();

	ctrl_state = WELCOME;
	tour = 0;
	uart_distance = 0;
	flag_arp1_timer = 0;
	flag_no_detection = 0;
	flag_rotation = 0;
	gyro_angle_arp1 = 0;
	flag_enable_measure = 1;
	error_distance = 10;
	go_left = 1;
	go_right = 0;
	motors_on = 0;
	tour_aux = 0;

	for(i = 0; i<MAX_TOUR; i++){
		arp1_tour[i].current_state = 0;
		arp1_tour[i].distance = 0;
	}
	xTaskCreate(arp1_task, "arp1_task", configMINIMAL_STACK_SIZE*2,
			NULL, tskIDLE_PRIORITY+1, &indicador);
	xTaskCreate(tx_task, "tx_task", configMINIMAL_STACK_SIZE*2,
			NULL, tskIDLE_PRIORITY+1, &indicador);

}

void give_semp_TX(void)
{
	static BaseType_t xAnother_task;
	xSemaphoreGiveFromISR(semp_uart_TX, &xAnother_task);
	portEND_SWITCHING_ISR(xAnother_task);
}
void give_semp_RX(void)
{
	static BaseType_t xAnother_task;
	xSemaphoreGiveFromISR(semp_uart_RX, &xAnother_task);
	portEND_SWITCHING_ISR(xAnother_task)
}


void data_rved(void * data, uint32_t len, char* p)
{
	memcpy(rx_buffer, data, len);
	uint8_t flag = 0;
	uint16_t number = 0;
	int8_t i, digit = 0;
	for (i = 0; i<10; i++){
		if(flag)
		{
			if (isdigit(*(p+i))){
				digit = *(p+i)-'0';
				number = number*10 + digit;
				if (!isdigit(*(p+i+1)))
					break;
			}
		}
		if(*(p+i) == ':')
			flag = 1;
	}
	uart_distance = number;
}

uint8_t dist_flag_rx(void)
{
	return uart_distance;
}

void go_to_patrol(void)
{
	motor_brake();
	motors_on = 0;
	tour++;
	tour_aux = tour;
	ctrl_state = PATROL;
	vTaskDelay(3000);
}

void turning_time(uint8_t direction)
{
	if(direction)
	{
		motor_turnLeft();
	}else{
		motor_turnRight();
	}
}

/*==================[end of file]============================================*/
