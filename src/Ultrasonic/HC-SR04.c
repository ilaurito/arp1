/* Copyright 2020, Grupo 5-TD2-FRH-UTN
 * All rights reserved.
 *
 * This file is part of the Final Proyect ARP1..
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*==================[inclusions]=============================================*/
#include "main.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "HC-SR04.h"

/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/
uint32_t IC_Val1;
uint32_t IC_Val2;
uint32_t Difference;
uint8_t Is_First_Captured;
uint8_t Distance;
TIM_HandleTypeDef htim2;

static SemaphoreHandle_t semp_HCSRO4;

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

/*==================[internal data declaration]==============================*/

static void HCSR04_Read_task(void *a)
{
	while(1){
		if(xSemaphoreTake(semp_HCSRO4, portMAX_DELAY) == pdTRUE){
			HAL_GPIO_WritePin(PIN_TRIGGER_GPIO_Port, PIN_TRIGGER_Pin, GPIO_PIN_RESET);
			delay(4);
			HAL_GPIO_WritePin(PIN_TRIGGER_GPIO_Port, PIN_TRIGGER_Pin, GPIO_PIN_SET);
			delay(10);
			HAL_GPIO_WritePin(PIN_TRIGGER_GPIO_Port, PIN_TRIGGER_Pin, GPIO_PIN_RESET);
			__HAL_TIM_ENABLE_IT(&htim2, TIM_IT_CC1);
		}
	}
}

void HCSR04_Init(void)
{

	IC_Val1 = 0;
	IC_Val2 = 0;
	Difference = 0;
	Is_First_Captured = 0;
	Distance  = 40;
	semp_HCSRO4 = xSemaphoreCreateBinary();
	TaskHandle_t indicador;
	xTaskCreate(HCSR04_Read_task, "HCSR04_Read_task", configMINIMAL_STACK_SIZE*1,
			NULL, tskIDLE_PRIORITY+3, &indicador);

}

void delay(uint16_t time)
{
	__HAL_TIM_SET_COUNTER(&htim2,0);
	while(__HAL_TIM_GET_COUNTER(&htim2)<time){}
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)  // if the interrupt source is channel1
	{
		if (Is_First_Captured==0) // if the first value is not captured
		{
			IC_Val1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1); // read the first value
			Is_First_Captured = 1;  // set the first captured as true
			// Now change the polarity to falling edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_FALLING);
		}

		else if (Is_First_Captured==1)   // if the first is already captured
		{
			IC_Val2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);  // read second value
			__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter

			if (IC_Val2 > IC_Val1)
			{
				Difference = IC_Val2-IC_Val1;
			}

			else if (IC_Val1 > IC_Val2)
			{
				Difference = (0xffff - IC_Val1) + IC_Val2;
			}

			//Distance = Difference * .034/2;
			Distance = Difference /58.3090379;
			Is_First_Captured = 0; // set it back to false

			// set polarity to rising edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_RISING);
			__HAL_TIM_DISABLE_IT(htim, TIM_IT_CC1);
		}
	}
}


void give_semp_HCSR04(void)
{
	xSemaphoreGive(semp_HCSRO4);
}

uint8_t HC_SR04_objeto_cercano(void)
{
	if(Distance <= 30){
		return 1;
	}
	else{
		return 0;
	}

}

uint8_t HC_SR04_distance(void)
{
	return Distance;
}
