/* Copyright 2020, Grupo 5-TD2-FRH-UTN
 * All rights reserved.
 *
 * This file is part of the Final Proyect ARP1..
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "mpu6050.h"
#include "main.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"


/*==================[macros and definitions]=================================*/

#define WHO_AM_I_REG	0x75
#define PWR_MGMT_REG	0x6B
#define CONFIG_REG		0x1A
#define SMPLRT_DIV_REG	0x19
#define GYRO_CONFIG_REG 0x1B
#define ACC_CONFIG_REG  0x1C
#define memAddrSize 	1

/*==================[internal data declaration]==============================*/
uint8_t drift_flag;
uint8_t data_read[6];
int16_t gyro_x_RAW;
int16_t gyro_y_RAW;
int16_t gyro_z_RAW;

int16_t gyro_x_CAL;
int16_t gyro_y_CAL;
int16_t gyro_z_CAL;

int16_t gx;
int16_t gy;
int16_t gz;

int32_t xcal;
int32_t ycal;
int32_t zcal;

float drift_z;
float angle_z;

static uint8_t devaddr = (0x68 << 1);

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

extern I2C_HandleTypeDef hi2c1;

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

static void gyroRead_Task(void *a){
	int i;
	while (1){
		if(drift_flag){

			drift_flag = 0;

			for(i = 0; i < 20; i++){
				HAL_I2C_Mem_Read(&hi2c1, devaddr, 0x43, memAddrSize, data_read , 6, 1000);

				gyro_x_RAW = (int16_t) (data_read[0] << 8 | data_read[1]);
				gyro_y_RAW = (int16_t) (data_read[2] << 8 | data_read[3]);
				gyro_z_RAW = (int16_t) (data_read[4] << 8 | data_read[5]);
				gz = (gyro_z_RAW - zcal);

				//drift_z += gz * 0.001374;
				//vTaskDelay(90);
				drift_z += gz * 0.000458;
				vTaskDelay(30);
			}
			drift_z /= 20;
			angle_z = 0;
			//give_semp_drift();
		} else {

			HAL_I2C_Mem_Read(&hi2c1, devaddr, 0x43, memAddrSize, data_read , 6, 1000);

			gyro_x_RAW = (int16_t) (data_read[0] << 8 | data_read[1]);
			gyro_y_RAW = (int16_t) (data_read[2] << 8 | data_read[3]);
			gyro_z_RAW = (int16_t) (data_read[4] << 8 | data_read[5]);


			gx = (gyro_x_RAW - xcal);
			gy = (gyro_y_RAW - ycal);
			gz = (gyro_z_RAW - zcal);

			//angulo_z += gz * 0.001374;
			angle_z += gz * 0.000458;
			angle_z -= drift_z;
			vTaskDelay(30);
			//vTaskDelay(90);
		}
	}
}

void gyro_init(I2C_HandleTypeDef *I2Cx){

	uint8_t check;
	uint8_t data;

	gyro_x_RAW = 0;
	gyro_y_RAW = 0;
	gyro_z_RAW = 0;

	angle_z = 0;
	drift_flag = 0;

	xcal = 0;
	ycal = 0;
	zcal = 0;
	HAL_I2C_Mem_Read(I2Cx, devaddr, WHO_AM_I_REG, 1, &check, 1, 1000); //WHO AM I REGISTER


	/*
	 * PWR MGMT 0x6B:
	 * 7: device reset, en 1 resetea todos los parametros del registro
	 * 6: sleep, 1 = deep sleep
	 * 5: cycle mode, cycles between sleep and waking up to take a single sample of data when sleep is disabled
	 * 4: -
	 * 3: temp sensor (1 deshabilitado)
	 * 2-0: CLK selection, 0 = 8 MHz internal OSC
	 *
	 * */

	data = 0;
	HAL_I2C_Mem_Write(I2Cx, devaddr, PWR_MGMT_REG, memAddrSize, &data, 1, 1000);

	/*reg 25, SMPLRT_DIV 0x19
	 * specifies the divider from the gyroscope output rate used to generate the sample rate
	 * sample rate = gyro output/(1 + smplrt_div)
	 * gyro output = 8khz cuando DLPF_CFG es 0 o 7
	 *
	 * 1khz sample, implica smplrt_div 0x07
	 */
	data = 0x07;
	HAL_I2C_Mem_Write(I2Cx, devaddr, SMPLRT_DIV_REG, memAddrSize, &data, 1, 1000);

	/*reg 27, gyro config 0x1B
	 * 765: Self test
	 * 4-3: full scale range 0 a 4, 250 500 1000 2000
	 * con plena escala 250, la salida va a ser 131 cuando se mueva 1 grado/s
	 * 					500, 65.5
	 * 					1000, 32.8
	 * 					2000, 16.4
	 */
	//data = 0x01;
	data = 0x08;
	HAL_I2C_Mem_Write(I2Cx, devaddr, GYRO_CONFIG_REG, memAddrSize, &data, 1, 1000);

	/*reg 28, acc config 0x1C
	 * 765: Self test
	 * 4-3: full scale range 0 a 4, 2g, 4g, 8g, 16g
	 */
	data = 0x00;
	HAL_I2C_Mem_Write(I2Cx, devaddr, ACC_CONFIG_REG, memAddrSize, &data, 1, 1000);
	TaskHandle_t indicador;
	xTaskCreate(gyroRead_Task, "gyroRead_Task", configMINIMAL_STACK_SIZE+1,
			NULL, tskIDLE_PRIORITY+1, &indicador);

	//reading registers  67 to 72, 0x43 to 0x48
	for (uint8_t i = 0; i < 200 ; i++){
		HAL_I2C_Mem_Read(&hi2c1, devaddr, 0x43, memAddrSize, data_read , 6, 1000);

		gyro_x_CAL = (int16_t) (data_read[0] << 8 | data_read[1]);
		gyro_y_CAL = (int16_t) (data_read[2] << 8 | data_read[3]);
		gyro_z_CAL = (int16_t) (data_read[4] << 8 | data_read[5]);

		xcal+= gyro_x_CAL;
		ycal+= gyro_y_CAL;
		zcal+= gyro_z_CAL;
	}
	xcal /= 200;
	ycal /= 200;
	zcal /= 200;

}


float gyro_angle(void)
{
	if(angle_z < 95 && angle_z > 85)
		return 90;
	if(angle_z < 5 && angle_z > -5)
		return 0;
	if(angle_z < 185 && angle_z > 175)
		return 180;
	if(angle_z < -85 && angle_z > -95)
		return -90;
	if(angle_z < -175 && angle_z > -185)
			return -180;
	if(angle_z < 275 && angle_z > 265)
				return 270;
	if(angle_z >= 360 || angle_z <= -360)
		angle_z = 0;
	return angle_z;
}
void gyro_angle_drift_fix(direction_states state)
{
	switch(state){
	case BACKWARD:
		angle_z = -90;
		break;
	case RIGHT:
		angle_z = 180;
		break;
	default:
		break;
	}
}

void gyro_drift_flag()
{
	drift_flag = 1;
}
