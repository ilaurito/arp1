/* Copyright 2016, Grupo 5-TD2-FRH-UTN
 * All rights reserved.
 *
 * This file is part of the Final Proyect ARP1.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** \addtogroup arp1 ARP1 machine
 ** @{ */
#ifndef INC_ARP1_H_
#define INC_ARP1_H_

#include "main.h"


/**
 * @brief ARP1's states
 */
typedef enum{
	WELCOME,		//!< WELCOME
	SETTINGS,		//!< SETTINGS
	WHERETO_TX,		//!< WHERETO_TX
	WHERETO_RX,		//!< WHERETO_RX
	HOWFAR_TX,		//!< HOWFAR_TX
	HOWFAR_RX,		//!< HOWFAR_RX
	BEGIN_TX,		//!< BEGIN_TX
	BEGIN_RX,		//!< BEGIN_RX
	PATROL,			//!< PATROL
	SCAN,			//!< SCAN
	ROTATION,		//!< ROTATION
	STOP			//!< STOP
}CTRL_STATES;

/**
 * @brief ARP1 direction's states
 */
typedef enum{
	FORWARD, 		//!< FORWARD
	BACKWARD,		//!< BACKWARD
	RIGHT,			//!< RIGHT
	LEFT			//!< LEFT
}direction_states;

/**
 * @brief	Initializes ARP1
 * @note 	This function creates tasks, Semaphores, Queues,  clears buffers,
 * 			and sets all flags to zero.
 */
void arp1_init(void);


/**
 * @brief This function gives a Semaphore
 */
void give_semp_TX(void);

/**
 * @brief This function gives a Semaphore
 */

void give_semp_RX(void);

/**
 * @brief  This function returns the value that was entered by the user
 * @return Distance entered by the user
 */
uint8_t dist_flag_rx(void);

/**
 * @brief       This function copies the information to send into the transmission buffer.
 * @param data	Information to send
 * @param len 	Size of data
 * @param p 	Pointer to a specified part of the buffer where a value can be extracted
 */
void data_rved(void * data, uint32_t len, char* p);

/**
 * @brief Counter for ARP1's timeout
 */
void arp1_tick(void);

/**
 * @brief This function changes the state of the robot
 */
void go_to_patrol(void);
/**
 * @brief This function turns the robot
 */
void turning_time(uint8_t);

/** @} doxygen end group definition */
#endif /* INC_ARP1_H_ */
