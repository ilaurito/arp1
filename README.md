# ARP1

Proyecto de Técnicas Digitales III
----------------------------------------------------------------------------------------------
 
Este proyecto es un robot patrulla, el cual puede recorrer un circuito cerrado predefinido por el usuario, deteniéndose en caso de tener un objeto cercano gracias a un sensor ultrasónico, y utilizando un sensor infrarrojo para detectar movmimiento. Para mantener la orientación y medir la distancia recorrida, se utilizan un giroscopio y un sensor de efecto hall. Se utiliza un puente-H para manejar 2 motores de corriente contínua.
Para definir el recorrido, el robot se conecta a un hub en la red local, el cual le puede programar un circuito fijo para que realice una vez enviada la instrucción "START". Alternativamente, se puede conectar a un servidor en una red externa, el cual puede cumplir la misma función que el hub. Al utilizar procesos padre/hijo, IPCs y sockets TCP y UDP, se pueden conectar varios de estos robots al mismo servidor.
Se puede encontrar un informe del mismo con una lista de componentes en el Informe Final: https://gitlab.com/ilaurito/arp1/-/blob/main/Informe-ARP1.pdf

----------------------------------------------------------------------------------------------
