/*********************************************************************************************************************
*Laurito Ignacio, Simonavicius Ivan
*                                       SERVIDOR ARP1
*
*                                       UTN FRH - 2021                                    
*
*********************************************************************************************************************/

/*============================[inclusions]=================================*/
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>

#define TCP_PORT 7575
#define BACKLOG 10
#define MAXBUFLEN 100


void handlerChild(int val);

/*==========================[internal data definition]====================*/
int timedout = 0, timedoutChild = 0;
int n, selret;
fd_set rfds;  
struct timeval tv;  
struct timeval tvChild;
int connection = 0;
int UDP_PORT = 10000;
int socketTCP_FD;
int newSocketTCP_FD;
int socketUDP_FD;
struct sockaddr_in myAddr;
struct sockaddr_in theirAddr;
struct sockaddr_in myAddrUDP;
struct sockaddr_in theirAddrUDP;
int  numbytes;
int  addr_len = sizeof(struct sockaddr);
char bufRead[MAXBUFLEN];
char bufWrite[MAXBUFLEN];
socklen_t sin_size = sizeof(struct sockaddr);
int pipes_fd[BACKLOG][2];
char *p;
int number;

int main()
{
    printf("************************************************************************\n");
    printf("Welcome to ARP1's simple server implementation\nMaximum connections: %d\n", BACKLOG);
    printf("************************************************************************\n");
    
    signal(SIGCHLD, handlerChild);

    /**************************************************************************
    *   Creando el socket
    *   (Abre via de comunicacion)
    **************************************************************************/
    if ((socketTCP_FD = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("TCP - Socket Error");
        exit(1);
    }

    /**************************************************************************
    *   Declaracion de la estructura myAddr para poder bindear al puerto
    *   (Da a conocer la direccion)
    ***************************************************************************/
    myAddr.sin_family = AF_INET;
    myAddr.sin_port = htons(TCP_PORT);
    myAddr.sin_addr.s_addr = inet_addr("192.168.0.76");
    bzero(&(myAddr.sin_zero), 8);

    /**************************************************************************
    *   Bind asocia al socket un numero de puerto y su direccion de IP
    ***************************************************************************/
    if (bind(socketTCP_FD, (struct sockaddr *) &myAddr, sizeof(struct sockaddr)) == -1) {
        perror ("TCP - Bind Error");
        exit(1);
    }

    /**************************************************************************
    *   Habilito para recibir conexiones, 10 como maximo
    ***************************************************************************/
    if (listen(socketTCP_FD, BACKLOG) == -1) {
        perror("TCP - Listen");
    }
    
    printf("\nSERVER: Waiting for connections...\n");
    /**************************************************************************
    *   Acepta una conexion sobre un socket (del tipo TCP)
    ***************************************************************************/
    while (!timedout) {
        while (connection >= 9) {
            printf("\nSERVER BUSY\n");
        }
        n = 0;
        n =  socketTCP_FD+1;
        
        FD_ZERO(&rfds);
        
        tv.tv_sec = 60;
        tv.tv_usec = 0;

        FD_SET(socketTCP_FD, &rfds);
        FD_SET(0, &rfds);

        if ((selret = select(n, &rfds, NULL, NULL, &tv)) == -1){
            //perror("PARENT SELECT");
        }

        if (FD_ISSET(socketTCP_FD, &rfds)) {
            if (selret == -1 && errno == EINTR) {
                //printf("PARENT: Bypassing error caused by SIGCHLD interrupting select\n");
                continue;
            }
            printf("PARENT: TCP Socket selected\n");
            if ((newSocketTCP_FD = accept(socketTCP_FD, (struct sockaddr *) &theirAddr, &sin_size)) == -1) {
                perror("TCP - Accept");
                continue;
            }
            printf("PARENT: Server conection %d from %s\n\n", connection, inet_ntoa(theirAddr.sin_addr));
            if ((socketUDP_FD = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
                perror ("UDP - Socket");
                exit(1);
            }
            printf("PARENT: UDP Socket Created for child %d\n", connection);
            myAddrUDP.sin_family = AF_INET;
            myAddrUDP.sin_port = htons (UDP_PORT);
            myAddrUDP.sin_addr.s_addr =inet_addr("192.168.0.76");
            bzero(&myAddrUDP.sin_zero, 8);
            if (bind(socketUDP_FD, (struct sockaddr *)&myAddrUDP, sizeof(struct sockaddr)) == -1) {
                perror("UDP - Bind");
                exit(1);
            }

            printf("PARENT: UDP IP-PORT Bound for child %d \n", connection);
            char mensaje[100];
            sprintf(mensaje, "%d", UDP_PORT);
            if ((send(newSocketTCP_FD, mensaje, sizeof(mensaje), 0)) == -1) {
                perror("TCP - Send");
            }

            printf("PARENT: UDP Port Sent to ARP1 #%d\n", connection);
            close(newSocketTCP_FD);
            printf("PARENT: TCP Socket closed\n");

            pipe(pipes_fd[connection]);
            if (!fork()) {
            /*************************************************************************    
            *   PROCESO HIJO
            *  _______________________________________________________________________
            *
            **************************************************************************/
                //close writing end of pipe
                close(pipes_fd[connection][1]);
                while (!timedoutChild) {
                    n = 0;
                    n = socketUDP_FD+1;
                    FD_ZERO(&rfds);
        
                    tv.tv_sec = 120;
                    tv.tv_usec = 0;

                    FD_SET(pipes_fd[connection][0], &rfds);
                    FD_SET(socketUDP_FD, &rfds);

                    if ((selret = select(n, &rfds, NULL, NULL, &tv)) == -1){
                        perror("CHILD: Select");
                    }

                    if (FD_ISSET(socketUDP_FD, &rfds)) {
                        if ((numbytes = recvfrom(socketUDP_FD, bufRead, MAXBUFLEN, 0, (struct sockaddr *)&theirAddrUDP, &addr_len))==-1) {
                            perror("UDP - Recv");
                            exit(1);
                        }
                        bufRead[numbytes] = '\0';
                        printf("FROM ARP1.%d: %s\n\n", connection, bufRead);
                        if(!strcmp(bufRead, "salir")) {
                            printf("Received command to terminate connection from ARP1.%d!", connection);
                            close(socketUDP_FD);
                            exit(0);
                        }
                    }

                    if (FD_ISSET(pipes_fd[connection][0], &rfds)) {
                        read(pipes_fd[connection][0], bufWrite, MAXBUFLEN);
                        if ((sendto(socketUDP_FD, bufWrite, strlen(bufWrite), 0, (struct sockaddr *)&theirAddrUDP, addr_len)) == -1) {
                            perror("UDP - Send");
                        }
                        if (strcmp(bufWrite, "salir") == 0) {
                            printf("Received commnd to terminate connection to ARP1.%d from stdin\n", connection);
                            close(socketUDP_FD);
                            exit(0);
                        } 
                        memset(bufWrite, 0, MAXBUFLEN);   
                    }

                    if (!selret) {
                        printf("\n=============================================================\n");
                        printf("CHILD %d: Server timed out. No packets received in 2 minutes.\n", connection);
                        if ((sendto(socketUDP_FD, "salir", strlen("salir"), 0, (struct sockaddr *)&theirAddrUDP, addr_len)) == -1) {
                            perror("UDP - Send");
                        }
                        close(socketUDP_FD);
                        printf("CHILD %d: Sockets closed. Exiting process...\n", connection);
                        printf("=============================================================\n\n");
                        exit(0);
                    }
                }
            }
            close(pipes_fd[connection][0]);
            connection++;
            UDP_PORT++;
        }

        if (FD_ISSET(0, &rfds)) {
            char buf[MAXBUFLEN];
            memset(buf, 0, MAXBUFLEN);
            fgets(bufWrite, sizeof(bufWrite), stdin);
            bufWrite[strcspn(bufWrite, "\n")] = 0;
            
            if((p = (char*)strstr(bufWrite, "ARP1.")) != NULL) {
                sscanf(bufWrite, "ARP1.%d %s", &number, buf);
                if(number <= connection) {
                    write(pipes_fd[number][1], buf, strlen(buf) + 1);
                    memset(buf, 0, MAXBUFLEN);
                } else {
                    printf("I don't know who to send this to\n");
                }
            }
            memset(bufWrite, 0, MAXBUFLEN);
            fflush(stdin);
        }

        if (!selret) {
            printf("\n\n********************************\nPARENT TIMEOUT. NOT SHUTTING DOWN, %d CONNECTIONS ALIVE\n********************************\n\n", connection);
        }

        if (!selret && !connection) {
            printf("\n=============================================================\n");
            printf("PARENT: Server timed out. \nNo new connections in 1 minute and no children active.\n");
            printf("=============================================================\n");
            timedout = 1;
            break;
        }   
    }
    printf("=============================================================\n");
    printf("\n\t\tPARENT: Process finished.\n\n");
    printf("=============================================================\n");
}

void handlerChild(int val)
{
    wait(0);
    connection--;
}