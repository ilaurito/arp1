/*********************************************************************************************************************
* PROYECTO ARP1
* MATERIA TECNICAS DIGITALES III
*
* LAURITO IGNACIO - SIMONAVICIUS IVAN
* UTN FRH - 2021                                    
*
*                                          CENTRO DE OPERACIONES
*********************************************************************************************************************/

/*********************************************************************************************************************
*                                       HEADERS
*********************************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <arpa/inet.h>

#define TCP_PORT_SERVER 7575
#define UDP_PORT_SERVER 2000
#define MAXDATASIZE 50
#define MAXBUFLEN 100
#define ARP1_PORT 55555
#define UDP_PORT_ARP1 8080

int pipeToSocket(int *, int *, struct sockaddr_in *);
int socketToPipe(int *, int *, struct sockaddr_in *);
void closePipes(void);
void socketTCP(int *, struct sockaddr_in *, int, char *);
void socketUDP(int *, struct sockaddr_in *, int, char *);

void handlerChild(int val);

int timedoutChild, count_child;
int n, selret;
fd_set rfds;
struct timeval tv;
struct timeval tvChild;
int numbytes;
int pipe1_fds[2];
int pipe2_fds[2];
int socketTCP_FD;
int socketUDP_FD;
int UDP_PORT;
struct sockaddr_in serverAddr, Arp1Addr, myAddr, myAddr2;
char bufRead[MAXBUFLEN];
char bufWrite[MAXBUFLEN];
char buf[MAXDATASIZE];
int addr_len = sizeof(struct sockaddr);
int socket_udp_arp1;

int main(int argc, char *argv[])
{
    int i;
    //setup pipes for ipc
    pipe(pipe1_fds); // RECEIVED messages from the SERVER (CASE 0 ON THE SWITCH CASE) will be WRITTEN onto pipe1. Afterwards, the pipe will be READ from the other process in order to send it to ARP1 (CASE 1 ON THE SWITCH CASE).
    pipe(pipe2_fds); // RECEIVED messages from ARP1 (CASE 1 ON THE SWITCH CASE) will be WRITTEN onto pipe2. Afterwards, the pipe will be READ from the other process in order to send it to the server (CASE 0 ON THE SWITCH CASE).

    timedoutChild = 0;
    count_child = 2;

    int ARP1_OUT_PORT = ARP1_PORT;
    Arp1Addr.sin_port = htons(ARP1_OUT_PORT);

    signal(SIGCHLD, handlerChild);
    for (i = 0; i < 2; i++)
    {
        if (!fork())
        {
            switch (i)
            {
            case 0:

                socketTCP(&socketTCP_FD, &serverAddr, TCP_PORT_SERVER, "200.122.56.128");

                if ((numbytes = recv(socketTCP_FD, buf, MAXDATASIZE, 0)) == -1)
                {
                    perror("TCP - Recv");
                    exit(1);
                }

                buf[numbytes] = '\0';
                printf("Puerto Recibido: %s\n", buf);
                close(socketTCP_FD);
                UDP_PORT = atoi(buf);
                serverAddr.sin_port = htons(UDP_PORT);

                socketUDP(&socketUDP_FD, &myAddr2, UDP_PORT_SERVER, "192.168.0.12");

                while (!timedoutChild)
                {
                    n = 0;
                    n = socketUDP_FD + 1;
                    FD_ZERO(&rfds);
                    tv.tv_sec = 120;
                    tv.tv_usec = 0;
                    FD_SET(socketUDP_FD, &rfds);
                    FD_SET(pipe2_fds[0], &rfds);

                    if ((selret = select(n, &rfds, NULL, NULL, &tv)) == -1)
                    {
                        perror("Select");
                        exit(1);
                    }
                    if (FD_ISSET(socketUDP_FD, &rfds))
                    {
                        if (socketToPipe(&pipe1_fds[1], &socketUDP_FD, &serverAddr))
                        {
                            timedoutChild = 1;
                            exit(0);
                        }
                    }
                    if (FD_ISSET(pipe2_fds[0], &rfds))
                    {
                        if (pipeToSocket(&pipe2_fds[0], &socketUDP_FD, &serverAddr))
                        {
                            timedoutChild = 1;
                            exit(0);
                        }
                    }
                    if (!selret)
                    {
                        printf("\n=============================================================\n");
                        printf(" \tTimed out. No packets received in 2 minutes.\n");
                        printf("=============================================================\n\n");
                        close(socketUDP_FD);
                        exit(0);
                    }
                }
                printf("\nI definitely should not be here\n");
                break;

            case 1:

                socketUDP(&socket_udp_arp1, &myAddr, UDP_PORT_ARP1, "192.168.0.12");
                printf("\n********\nWaiting for packet... \n********\n");
                while (!timedoutChild)
                {
                    n = 0;
                    n = pipe1_fds[0] + 1;
                    if (n < socket_udp_arp1)
                    {
                        n = socket_udp_arp1 + 1;
                    }
                    FD_ZERO(&rfds);
                    tv.tv_sec = 120;
                    tv.tv_usec = 0;
                    FD_SET(socket_udp_arp1, &rfds);
                    FD_SET(pipe1_fds[0], &rfds);
                    FD_SET(0, &rfds);
                    if ((selret = select(n, &rfds, NULL, NULL, &tv)) == -1)
                    {
                        perror("Select");
                    }

                    if (FD_ISSET(0, &rfds))
                    {
                        fgets(bufWrite, sizeof(bufWrite), stdin);
                        bufWrite[strcspn(bufWrite, "\n")] = 0;
                        write(pipe2_fds[1], bufWrite, strlen(bufWrite) + 1);
                        if (!(strcmp(bufWrite, "salir")))
                        {
                            timedoutChild = 1;
                            exit(0);
                        }

                        fflush(stdin);
                    }

                    if (FD_ISSET(socket_udp_arp1, &rfds))
                    {
                        if (socketToPipe(&pipe2_fds[1], &socket_udp_arp1, &Arp1Addr))
                        {
                            timedoutChild = 1;
                            exit(0);
                        }
                    }

                    if (FD_ISSET(pipe1_fds[0], &rfds))
                    {
                        if (pipeToSocket(&pipe1_fds[0], &socket_udp_arp1, &Arp1Addr))
                        {
                            timedoutChild = 1;
                            exit(0);
                        }
                    }
                    if (!selret)
                    {
                        printf("\n=============================================================\n");
                        printf("\tTimed out. No packets received in 2 minutes.\n");
                        printf("=============================================================\n\n");
                        close(socket_udp_arp1);
                        exit(0);
                    }
                }
                break;
            }
        }
    }
    while (count_child)
    {
    }
    timedoutChild = 0;
    closePipes();
}

int pipeToSocket(int *pipe, int *socket, struct sockaddr_in *addr)
{
    char buf[MAXBUFLEN];
    memset(buf, 0, MAXBUFLEN);
    read(*pipe, buf, MAXBUFLEN);
    if (sendto(*socket, buf, strlen(buf), 0, (struct sockaddr *)addr, addr_len) == -1)
    {
        perror("UDP - Send from pipe");
        exit(1);
    }
    if (!(strcmp(buf, "salir")))
    {
        return 1;
    }
    return 0;
}

int socketToPipe(int *pipe, int *socket, struct sockaddr_in *addr)
{
    char buf[MAXBUFLEN];
    int numbytes2;
    memset(buf, 0, MAXBUFLEN);

    if ((numbytes2 = recvfrom(*socket, buf, MAXBUFLEN, 0, (struct sockaddr *)addr, &addr_len)) == -1)
    {
        perror("UDP - Recv (send to pipe)");
        exit(1);
    }

    buf[numbytes2] = '\0';
    printf("\n%s: %s\n", inet_ntoa(addr->sin_addr), buf);
    write(*pipe, buf, strlen(buf) + 1);

    if (!(strcmp(buf, "salir\0")))
    {
        return 1;
    }
    return 0;
}

void closePipes(void)
{
    printf("\n=============================================================\n");
    printf("\t\t  Shutting down Server\n");
    printf("=============================================================\n\n");

    close(pipe1_fds[0]);
    close(pipe1_fds[1]);
    close(pipe2_fds[0]);
    close(pipe2_fds[1]);
}

void socketTCP(int *sock, struct sockaddr_in *addr, int port, char *ip)
{
    //STEP 1, create TCP socket
    if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("TCP - Socket");
        exit(1);
    }
    printf("TCP Socket created\n");

    //STEP 2, connect to server
    addr->sin_family = AF_INET;
    addr->sin_port = htons(port);
    addr->sin_addr.s_addr = inet_addr(ip);
    memset(addr->sin_zero, 0, 8);
    if (connect(*sock, (struct sockaddr *)addr, sizeof(struct sockaddr)) == -1)
    {
        perror("TCP - Connect");
        exit(1);
    }
    printf("TCP Socket connected\n");
}

void socketUDP(int *sock, struct sockaddr_in *addr, int port, char *ip)
{
    addr->sin_family = AF_INET;
    addr->sin_port = htons(port);
    addr->sin_addr.s_addr = inet_addr(ip);
    memset(addr->sin_zero, 0, 8);

    if ((*sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("UDP - Socket");
        exit(1);
    }
    if (bind(*sock, (struct sockaddr *)addr, sizeof(struct sockaddr)) == -1)
    {
        perror("UDP - Bind");
        exit(1);
    }
    printf("UDP IP-PORT Bound puerto: %d\n", port);
}

void handlerChild(int val)
{
    wait(NULL);
    count_child--;
}